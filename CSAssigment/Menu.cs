﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSAssigment
{
    public enum Main
    {
        Show_All,
        Drink,
        Food,
        Dessert,
        Delete_Meal,
        Checkout,
        Exit
    }
    public class Menu
    {
        
        public Cart cart = new Cart();
        public List<Meal>   meals = Meal.GetMeals();
        public void AddMeal(Meal m)
        {
            cart.Order.Add(m);
        }

        public void RemoveMeal(Meal m)
        {
            cart.Order.Remove(m);
        }

        public void ShowMenu(string m) {
            if (m == null)
            {
                ShowMainMenu();
            }
            else
            {
                if (m == Main.Delete_Meal.ToString())
                {
                    ShowOrderMenu();
                    return;
                }
                if (m == Main.Checkout.ToString())
                {
                    GiveThanks();
                    return;
                }
                ShowFilteredMenu(Convert.ToInt32(m));
            }
        }

        private void GiveThanks()
        {
            Console.Clear();
            Console.WriteLine(@"
================================
          Thank You                                
================================
b - Back
");
        }

        private void ShowFilteredMenu(int type)
        {
            Console.Clear();
            Console.WriteLine(@"
================================
              Menu                                
================================
b - Back
" 

+
string.Format(@"
{0}
", string.Join("\n", meals.Where(m=> (int)m.Type==type||type==(int)Main.Show_All).Select(m=>string.Format("{0} - {1}\t\t{2:C}",meals.IndexOf(m)+1,m.Name,m.Price)).ToList()))
+
cart.CartStatus()
+ @"
Add Meal:");
        }
        private void ShowOrderMenu()
        {
            Console.Clear();
            Console.WriteLine(@"
================================
              Menu                                
================================
b - Back
"

+
string.Format(@"
{0}
", string.Join("\n", cart.Order.Select(m => string.Format("{0} - {1}\t\t{2:C}", cart.Order.IndexOf(m) + 1, m.Name, m.Price)).ToList()))
+ @"
Add Meal:");
        }

        private void ShowMainMenu()
        {
            Console.Clear();
            Console.WriteLine(@"
================================
              Menu                                
================================
" +
string.Join("\n",(Enum.GetValues(typeof(Main)).Cast<Main>().Select(x=>string.Format("{0} - {1}",(int)x,x.ToString().Replace('_',' ')))))
+
cart.CartStatus()
+ @"
Select option:");
        }
    }
}
