﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CSAssigment
{
    public class Meal : ICloneable
    {
        public Main Type { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public static List<Meal> GetMeals() {
            return new List<Meal>()
            {
                new Meal(){ Type=Main.Drink,Name="Agua fresca",Price=1.0},
                new Meal(){ Type=Main.Food,Name="Breakfast",Price=10.0},
                new Meal(){ Type=Main.Dessert,Name="Candy",Price=5.0},
                new Meal(){ Type=Main.Drink,Name="Soda",Price=2.0},
                new Meal(){ Type=Main.Food,Name="Lunch",Price=15.0},
                new Meal(){ Type=Main.Dessert,Name="Icecream",Price=9.0}
                
            };
        }
        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }

}
