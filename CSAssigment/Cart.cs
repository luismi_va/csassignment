﻿namespace CSAssigment
{
    public class Cart
    {
        public List<Meal> Order { get; set; }
        public Cart() { 
        Order=new List<Meal>();
        }
        public string CartStatus()
        {
            return string.Format("\nMeal Count\t\t<{0}>\n\nTotal:\t\t\t{1:C}\n", Order.Count, Order.Select(m => m.Price).ToArray().Sum());
        }
    }
}