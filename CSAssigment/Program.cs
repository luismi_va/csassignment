﻿// See https://aka.ms/new-console-template for more information
using CSAssigment;
using System.ComponentModel.DataAnnotations;

string input = null;
var op = -1;
Menu m = new Menu();

do {
    var oldInput = input;
    m.ShowMenu(input);
    input = Console.ReadLine();
    try
    {
        op = Convert.ToInt16(input);
    }catch (Exception ex)
    {
        op = -1;
        input = null;
    }
    if (input == null)
    {
        switch (op)
        {
            case (int)Main.Checkout:
                input = Main.Checkout.ToString();
                break;
            case (int)Main.Delete_Meal:
                input = Main.Delete_Meal.ToString();
                break;
        }
    }
    else
    {
        if (oldInput != null)
        {
            
            try
            {
                if (oldInput!= Main.Delete_Meal.ToString())
                {
                    Meal meal = m.meals.ElementAt(op - 1);
                    m.AddMeal((Meal)meal.Clone());

                }
                else
                {
                    Meal meal = m.cart.Order.ElementAt(op - 1);
                    m.RemoveMeal(meal);

                }
                if (op == (int)Main.Exit)
                {
                    op = -1;
                }
            }
            finally
            {
                input = null;
            }
        }
        else
        {
            
            if (op == (int)Main.Checkout && m.cart.Order.Count>0)
            {
                m.cart.Order.Clear();
                input = Main.Checkout.ToString();
            }
            else
            {
                if (op == (int)Main.Delete_Meal)
                {
                    input = Main.Delete_Meal.ToString();
                }
            }
        }
        
    }
    
} while (op!=(int)Main.Exit);